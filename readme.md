#Install
Clone this repo
git clone git@bitbucket.org:stefan_matei/oauthserver.git

Run the following commands in order from the root directory:
- composer install
- php vendor/bin/homestead make

Edit Homestead.yaml to suit your needs. Homestead.sample.yaml is provided as reference

#Start app
- vagrant up
- vagrant ssh


#On the machines
Always pull the latest version. 
- git pull origin master

Install dependencies
- composer install

Run migrations.
- php bin/console doctrine:migrations:migrate