<?php

namespace App\Controller;

use App\Repository\LeadRepository;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/protected", name="api_protected")
     * Test page for Zapier integration
     */
    public function index(): JsonResponse
    {
        return $this->json(['data' => 'success']);
    }

    /**
     * @Route("/api/lead", name="api_new_lead", methods={"GET"})
     * @return JsonResponse
     */
    public function newLead(
        LeadRepository $leadRepository,
        \JMS\Serializer\SerializerInterface $serializer
    ): JsonResponse {
        /**
         * Here enter the logic to retrieve a new LEAD
         */

        $leads = $leadRepository->getNewLeads();

        $data = json_decode(
            $serializer->serialize(
                $leads,
                'json',
                SerializationContext::create()->setGroups(['newLead'])->setSerializeNull(true)
            ),
            true
        );

        return $this->json($data);
    }


}
