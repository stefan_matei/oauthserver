<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\SetupOauthClientType;
use App\Model\OauthClientFactory;
use Doctrine\Persistence\ManagerRegistry;
use League\Bundle\OAuth2ServerBundle\Manager\Doctrine\ClientManager;
use League\Bundle\OAuth2ServerBundle\Model\RedirectUri;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    /**
     * @Route("/profile", name="profile")
     */
    public function index(ClientManager $clientManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $oauthClient = $clientManager->find($user->getOauthIdentifier());

        $data = [];
        if ($oauthClient) {
            $uris = $oauthClient->getRedirectUris();
            $data['redirectUri'] = reset($uris);
        }

        $form = $this->createClientSetupForm($data);

        return $this->render(
            'profile/index.html.twig',
            [
                'user' => $user,
                'oauthClient' => $oauthClient,
                'form' => $form->createView(),
            ]
        );
    }

    protected function createClientSetupForm(array $data): FormInterface
    {
        $form = $this->createForm(
            SetupOauthClientType::class,
            $data,
            [
                'action' => $this->generateUrl('oauth_setup'),
                'method' => 'POST',
            ]
        );

        $form->add('submit', SubmitType::class);

        return $form;
    }

    /**
     * @Route("/setup_oauth", name="oauth_setup", methods={"POST"})
     */
    public function setupOauthAccount(
        Request $request,
        ClientManager $clientManager,
        OauthClientFactory $clientFactory,
        ManagerRegistry $registry
    ) {
        /** @var User $user */
        $user = $this->getUser();

        $oauthClient = $clientManager->find($user->getOauthIdentifier());

        if (!$oauthClient) {
            $oauthClient = $clientFactory->getNewClient();
        }

        $data = [];

        $uris = $oauthClient->getRedirectUris();
        $data['redirectUri'] = reset($uris);

        $form = $this->createClientSetupForm($data);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $redirectUri = new RedirectUri($form->get('redirectUri')->getData());
            $oauthClient->setRedirectUris($redirectUri);

            $clientManager->save($oauthClient);
            $user->setOauthIdentifier($oauthClient->getIdentifier());

            $em = $registry->getManagerForClass(User::class);
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Client created');

            return $this->redirectToRoute('profile');
        }

        $this->addFlash('error', 'Client could not be saved');

        return $this->redirectToRoute('profile');
    }
}
