<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use League\Bundle\OAuth2ServerBundle\Event\AuthorizationRequestResolveEvent;

class AuthorizationRequestResolveSubscriber implements EventSubscriberInterface
{
    public function onOauth2AuthorizationRequest(AuthorizationRequestResolveEvent $event)
    {
        $event->resolveAuthorization(true);
    }

    public static function getSubscribedEvents()
    {
        return [
            'league.oauth2-server.authorization_request_resolve' => 'onOauth2AuthorizationRequest',
        ];
    }
}
