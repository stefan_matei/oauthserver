<?php


namespace App\Repository;


use App\Faker\LeadGenerator;
use App\Model\LeadRepositoryInterface;

class LeadRepository implements LeadRepositoryInterface
{

    /**
     *
     * TO BE REPLACED WITH AN ACTUAL SOURCE OF LEADS. PROBABLY ENTITY MANAGER.
     * @var LeadGenerator
     */
    private $leadGenerator;

    /**
     * LeadRepository constructor.
     * @param LeadGenerator $leadGenerator
     */
    public function __construct(LeadGenerator $leadGenerator)
    {
        $this->leadGenerator = $leadGenerator;
    }

    public function getNewLeads(): array
    {
        $max = random_int(0, 11);

        $leads = [];
        /**
         * Replace this for loop with a DB QUERY
         */
        for ($i = 1; $i <= $max; $i++) {
            $leads[] = $this->leadGenerator->generateLead();
        }

        return $leads;
    }


}