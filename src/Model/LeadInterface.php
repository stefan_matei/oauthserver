<?php


namespace App\Model;


interface LeadInterface
{

    /*
    *  {
  "runId": 43060,
  "firstName": "John",
  "lastName": "Doe",
  "name": "John Doe",
  "email": "test@test.com",
  "gender": "m",
  "birthday": "2021-01-06",
  "age": 55,
  "streetAddress": "1 Northeastern Blvd, Apt 2, Apt 2",
  "zipCode": 3109,
  "city": "New York",
  "state": "NY",
  "country": "US",
  "phone": 83434353543,
  "maritalStatus": "separated",
  "relationshipStatus": "casual relationship",
  "smallText1": "Lorem ipsum dolor sit amet, consectetur adipiscing",
  "smallText2": "Lorem ipsum dolor sit amet, consectetur adipiscing",
  "smallText3": "Lorem ipsum dolor sit amet, consectetur adipiscing",
  "smallText4": "Lorem ipsum dolor sit amet, consectetur adipiscing",
  "largeText1": "The standard Lorem Ipsum passage, used since the 1500s\\n\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nis",
  "largeText2": "The standard Lorem Ipsum passage, used since the 1500s\\n\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nis",
  "largeText3": "The standard Lorem Ipsum passage, used since the 1500s\\n\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nis",
  "largeText4": "The standard Lorem Ipsum passage, used since the 1500s\\n\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nis",
  "c1": "subid1",
  "c2": "subid2",
  "c3": "subid3",
  "answer": "The standard Lorem Ipsum passage, used since the 1500s\\n\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nis",
}
    */

    public function getId(): ?int; // required by Zapier

    public function getRunId(): ?int;

    public function getFirstName(): ?string;

    public function getLastName(): ?string;

    public function getName(): ?string;

    public function getEmail(): ?string;

    public function getGender(): ?string;

    public function getBirthday(): ?string;

    public function getAge(): ?int;

    public function getStreetAddress(): ?string;

    public function getZipCode(): ?string;

    public function getCity(): ?string;

    public function getState(): ?string;

    public function getCountry(): ?string;

    public function getPhone(): ?string;

    public function getMaritalStatus(): ?string;

    public function getRelationshipStatus(): ?string;

    public function getSmallText1(): ?string;

    public function getSmallText2(): ?string;

    public function getSmallText3(): ?string;

    public function getSmallText4(): ?string;

    public function getLargeText1(): ?string;

    public function getLargeText2(): ?string;

    public function getLargeText3(): ?string;

    public function getLargeText4(): ?string;

    public function getFullAddress(): string;

    public function getAnswer(): ?string;

    public function getC3(): ?string;

    public function getC2(): ?string;

    public function getC1(): ?string;

}