<?php


namespace App\Model;


use JMS\Serializer\Annotation as JMS;
/**
 * Class Lead
 * @package App\Model
 *
 * @JMS\ExclusionPolicy("all")
 */
class Lead implements \JsonSerializable, LeadInterface
{

    /**
     * @var int|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     *
     */
    private $runId;

    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     *
     */
    private $firstName;

    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     *
     */
    private $lastName;

    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     *
     */
    private $name;

    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     *
     */
    private $email;

    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $gender;

    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $birthday;

    /**
     * @var int|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     *
     */
    private $age;

    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $streetAddress;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $zipCode;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $city;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $state;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $country;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $phone;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $maritalStatus;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $relationshipStatus;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $smallText1;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $smallText2;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $smallText3;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $smallText4;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $largeText1;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $largeText2;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $largeText3;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $largeText4;

    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $c1;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $c2;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $c3;
    /**
     * @var string|null
     * @JMS\Expose()
     * @JMS\Groups({"newLead"})
     */
    private $answer;

    /**
     * @return int|null
     *
     * @JMS\Expose()
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Groups({"newLead"})
     */
    public function getId(): ?int
    {
        return $this->getRunId();
    }

    /**
     * @return int|null
     */
    public function getRunId(): ?int
    {
        return $this->runId;
    }

    /**
     * @param int|null $runId
     * @return Lead
     */
    public function setRunId(?int $runId): Lead
    {
        $this->runId = $runId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     * @return Lead
     */
    public function setFirstName(?string $firstName): Lead
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     * @return Lead
     */
    public function setLastName(?string $lastName): Lead
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Lead
     */
    public function setName(?string $name): Lead
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return Lead
     */
    public function setEmail(?string $email): Lead
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @param string|null $gender
     * @return Lead
     */
    public function setGender(?string $gender): Lead
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBirthday(): ?string
    {
        return $this->birthday;
    }

    /**
     * @param string|null $birthday
     * @return Lead
     */
    public function setBirthday(?string $birthday): Lead
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getAge(): ?int
    {
        return $this->age;
    }

    /**
     * @param int|null $age
     * @return Lead
     */
    public function setAge(?int $age): Lead
    {
        $this->age = $age;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStreetAddress(): ?string
    {
        return $this->streetAddress;
    }

    /**
     * @param string|null $streetAddress
     * @return Lead
     */
    public function setStreetAddress(?string $streetAddress): Lead
    {
        $this->streetAddress = $streetAddress;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    /**
     * @param string|null $zipCode
     * @return Lead
     */
    public function setZipCode(?string $zipCode): Lead
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return Lead
     */
    public function setCity(?string $city): Lead
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @param string|null $state
     * @return Lead
     */
    public function setState(?string $state): Lead
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return Lead
     */
    public function setCountry(?string $country): Lead
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return Lead
     */
    public function setPhone(?string $phone): Lead
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMaritalStatus(): ?string
    {
        return $this->maritalStatus;
    }

    /**
     * @param string|null $maritalStatus
     * @return Lead
     */
    public function setMaritalStatus(?string $maritalStatus): Lead
    {
        $this->maritalStatus = $maritalStatus;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRelationshipStatus(): ?string
    {
        return $this->relationshipStatus;
    }

    /**
     * @param string|null $relationshipStatus
     * @return Lead
     */
    public function setRelationshipStatus(?string $relationshipStatus): Lead
    {
        $this->relationshipStatus = $relationshipStatus;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSmallText1(): ?string
    {
        return $this->smallText1;
    }

    /**
     * @param string|null $smallText1
     * @return Lead
     */
    public function setSmallText1(?string $smallText1): Lead
    {
        $this->smallText1 = $smallText1;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSmallText2(): ?string
    {
        return $this->smallText2;
    }

    /**
     * @param string|null $smallText2
     * @return Lead
     */
    public function setSmallText2(?string $smallText2): Lead
    {
        $this->smallText2 = $smallText2;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSmallText3(): ?string
    {
        return $this->smallText3;
    }

    /**
     * @param string|null $smallText3
     * @return Lead
     */
    public function setSmallText3(?string $smallText3): Lead
    {
        $this->smallText3 = $smallText3;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSmallText4(): ?string
    {
        return $this->smallText4;
    }

    /**
     * @param string|null $smallText4
     * @return Lead
     */
    public function setSmallText4(?string $smallText4): Lead
    {
        $this->smallText4 = $smallText4;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLargeText1(): ?string
    {
        return $this->largeText1;
    }

    /**
     * @param string|null $largeText1
     * @return Lead
     */
    public function setLargeText1(?string $largeText1): Lead
    {
        $this->largeText1 = $largeText1;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLargeText2(): ?string
    {
        return $this->largeText2;
    }

    /**
     * @param string|null $largeText2
     * @return Lead
     */
    public function setLargeText2(?string $largeText2): Lead
    {
        $this->largeText2 = $largeText2;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLargeText3(): ?string
    {
        return $this->largeText3;
    }

    /**
     * @param string|null $largeText3
     * @return Lead
     */
    public function setLargeText3(?string $largeText3): Lead
    {
        $this->largeText3 = $largeText3;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLargeText4(): ?string
    {
        return $this->largeText4;
    }

    /**
     * @param string|null $largeText4
     * @return Lead
     */
    public function setLargeText4(?string $largeText4): Lead
    {
        $this->largeText4 = $largeText4;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getC1(): ?string
    {
        return $this->c1;
    }

    /**
     * @param string|null $c1
     * @return Lead
     */
    public function setC1(?string $c1): Lead
    {
        $this->c1 = $c1;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getC2(): ?string
    {
        return $this->c2;
    }

    /**
     * @param string|null $c2
     * @return Lead
     */
    public function setC2(?string $c2): Lead
    {
        $this->c2 = $c2;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getC3(): ?string
    {
        return $this->c3;
    }

    /**
     * @param string|null $c3
     * @return Lead
     */
    public function setC3(?string $c3): Lead
    {
        $this->c3 = $c3;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    /**
     * @param string|null $answer
     * @return Lead
     */
    public function setAnswer(?string $answer): Lead
    {
        $this->answer = $answer;

        return $this;
    }

    public function getFullAddress(): string
    {
        return implode(
            ',',
            [
                $this->country,
                $this->city,
                $this->streetAddress,
                $this->zipCode,
            ]
        );
    }


    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }


}