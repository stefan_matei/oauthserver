<?php


namespace App\Model;


use League\Bundle\OAuth2ServerBundle\Model\Client;
use League\Bundle\OAuth2ServerBundle\Model\Grant;
use League\Bundle\OAuth2ServerBundle\Model\RedirectUri;

class OauthClientFactory
{



    public function getNewClient(): Client
    {

        $identifier = hash('md5', random_bytes(16));
        $secret = hash('sha512', random_bytes(32));

        $client = new Client($identifier, $secret);
        $client->setActive(true);

        $grant  = new Grant('authorization_code');
        $client->setGrants($grant);




        return $client;
    }

}