<?php


namespace App\Model;


interface LeadRepositoryInterface
{

    /**
     *
     * Returns an array of new leads or an empty array if no new lead is available
     *
     * @return LeadInterface[]|[]
     */
    public function getNewLeads(): array;


}