<?php


namespace App\Faker;


use App\Model\Lead;
use App\Model\LeadInterface;
use Faker\Factory;
use Faker\Generator;

class LeadGenerator
{

    /**
     * @var Generator
     */
    private $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    /**
     * @return LeadInterface
     * @throws \Exception
     *
     * {
     * "runId": 43060,
     * "firstName": "John",
     * "lastName": "Doe",
     * "name": "John Doe",
     * "email": "test@test.com",
     * "gender": "m",
     * "birthday": "2021-01-06",
     * "age": 55,
     * "streetAddress": "1 Northeastern Blvd, Apt 2, Apt 2",
     * "zipCode": 3109,
     * "city": "New York",
     * "state": "NY",
     * "country": "US",
     * "phone": 83434353543,
     * "maritalStatus": "separated",
     * "relationshipStatus": "casual relationship",
     * "smallText1": "Lorem ipsum dolor sit amet, consectetur adipiscing",
     * "smallText2": "Lorem ipsum dolor sit amet, consectetur adipiscing",
     * "smallText3": "Lorem ipsum dolor sit amet, consectetur adipiscing",
     * "smallText4": "Lorem ipsum dolor sit amet, consectetur adipiscing",
     * "largeText1": "The standard Lorem Ipsum passage, used since the 1500s\\n\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nis",
     * "largeText2": "The standard Lorem Ipsum passage, used since the 1500s\\n\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nis",
     * "largeText3": "The standard Lorem Ipsum passage, used since the 1500s\\n\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nis",
     * "largeText4": "The standard Lorem Ipsum passage, used since the 1500s\\n\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nis",
     * "c1": "subid1",
     * "c2": "subid2",
     * "c3": "subid3",
     * "answer": "The standard Lorem Ipsum passage, used since the 1500s\\n\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nis",
     * }
     */
    public function generateLead(): LeadInterface
    {
        $lead = new Lead();
        $lead->setFirstName($this->faker->firstName);
        $lead->setLastName($this->faker->lastName);
        $lead->setName($lead->getFirstName().' '.$lead->getLastName());
        $lead->setPhone($this->faker->phoneNumber);
        $lead->setAge(random_int(18, 70));
        $lead->setAnswer($this->faker->text);
        $lead->setBirthday($this->faker->date('Y-m-d', '-18 years'));
        $lead->setC1('subid1');
        $lead->setC2('subid2');
        $lead->setC3('subid3');
        $lead->setCity($this->faker->city);
        $lead->setCountry($this->faker->countryCode);
        $lead->setEmail($this->faker->email);
        $lead->setGender('m');
        $lead->setLargeText1($this->faker->text);
        $lead->setLargeText2($this->faker->text);
        $lead->setLargeText3($this->faker->text);
        $lead->setLargeText4($this->faker->text);
        $lead->setMaritalStatus('separated');
        $lead->setRunId(random_int(1000, 100000));
        $lead->setRelationshipStatus('casual relationship');
        $lead->setSmallText1($this->faker->text);
        $lead->setSmallText2($this->faker->text);
        $lead->setSmallText3($this->faker->text);
        $lead->setSmallText4($this->faker->text);
        $lead->setState($this->faker->state);
        $lead->setStreetAddress($this->faker->streetAddress);
        $lead->setZipCode($this->faker->postcode);

        return $lead;
    }

}